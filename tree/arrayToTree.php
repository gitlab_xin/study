<?php
/**
 * Created by PhpStorm.
 * User: macxin
 * Date: 2020/4/19
 * Time: 1:52 下午
 */

$array = [1,3,2,5];
$tree = new CreateTree();
$result = $tree->arrayToTree($array, 0);
header("Content-type:text/json");
var_dump($result);

class TreeNode {
 public $val = null;
 public $left = null;
 public $right = null;
 function __construct($value) { $this->val = $value; }
}


class CreateTree {

    function arrayToTree($array, $index){

        $root = null;
        if ($index < count($array)) {
            $value = $array[$index];
            if ($value == null) {
                return null;
            }
            $root = new TreeNode($value);
            $root->left =  self::arrayToTree($array, 2*$index + 1);
            $root->right =  self::arrayToTree($array, 2*$index + 2);

            return $root;
        }

        return $root;

    }


}